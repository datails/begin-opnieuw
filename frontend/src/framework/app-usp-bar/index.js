import React from "react";
import { Grid, createStyles, makeStyles, Typography } from "@material-ui/core";
import AppUSPCard from "../../components/app-usp-bar";
import AppButton from "../../components/app-button";

const useStyles = makeStyles((theme) =>
  createStyles({
    container: {
      margin: "0",
      width: "100%",
      padding: "80px 40px",
      background: "#ffe5ca",
      [theme.breakpoints.down("sm")]: {
        padding: "40px 0",
      },
    },
    content: {
      display: "flex",
      justifyContent: "center",
    },
    title: {
      marginBottom: "50px",
    },
  })
);

export default function ComponentTopBanner() {
  const classes = useStyles();

  return (
    <Grid
      container
      className={classes.container}
      justify="space-evenly"
      spacing={10}
    >
      <Grid item lg={12} xl={8} justify="center" align="center">
        <Grid container spacing={10} justify="space-evenly">
          <Grid item sm={12} justify="center" align="center">
            <Typography
              variant="h2"
              component="h2"
              gutterBottom
              className={classes.title}
            >
              Het Koninkrijk van God
            </Typography>
            <Typography variant="subtitle1" component="h3">
              Stel je sterft vannacht, en je komt voor Jezus te staan, en Hij vraagt jou: “Heb je alles gedaan wat in je mogelijkheden lag, om Mijn wil te doen?”. Heb je je werkelijk ingespannen voor je redding? Heb je er werkelijk alles aangedaan om een discipel van Mij te zijn? En heb je je geestelijke talenten gebruikt ter opbouw van Mijn gemeente?
              <br />Wij moeten er voor zorgen dat we klaar staan op het moment dat onze tijd gekomen is.
            </Typography>
          </Grid>
          <Grid item md={12} lg={6} className={classes.content}>
            <AppUSPCard
              background="repentance.jpg"
              desc="Werkelijke bekering is dat je, je oude leven achter je laat en bewust afstand neemt van je zondige natuur."
              title="Bekering"
            >
              1
            </AppUSPCard>
          </Grid>
          <Grid item md={12} lg={6} className={classes.content}>
            <AppUSPCard
              background="baptism.jpg"
              desc="De doop is de begrafenis van je oude leven, waarmee je opstaat in een nieuw leven. Jij bent nu niet meer heer van je leven, mij Hij is HEER van jou leven."
              title="Doop in water"
            >
              2
            </AppUSPCard>
          </Grid>
          <Grid item md={12} lg={6} className={classes.content}>
            <AppUSPCard
              background="spirit.jpg"
              desc="Waarom noem je mij Heere Heere, en doe je niet wat ik zeg? De Geest geeft ons kracht om te doen wat Jezus van ons vraagt."
              title="Doop in Geest"
            >
              3
            </AppUSPCard>
          </Grid>
          <Grid item sm={12} justify="center" align="center">
            <AppButton
              styles={{
                "min-width": "250px",
                "font-size": "1.1rem",
                height: "60px",
              }}
              href="/teachings"
            >
              Teachings
            </AppButton>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}
