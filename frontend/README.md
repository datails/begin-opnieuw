# Traveling Seven

## CMS
https://travelingseven.herokuapp.com/admin/

A webhook is configured in Strapi that triggers on each change a new GitLab publish. We then lookup what has changed in strapi from the `pipeline` directory and fetches the latest data and do a data dump in `data` dir insinde `src`. We then do a new build.